package br.com.itau.hello;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@GetMapping("/ola")
	public String mostrarOla() {
		String texto = "<h1>Olá mundo</h1>";
		texto += "<a href=''>Tchau</a>";
		
		return texto;
	}
	
	@GetMapping("/tchau")
	public String mostrarTchau() {
		return "<h1>Tchau mundo</h1>";
	}
	
}
